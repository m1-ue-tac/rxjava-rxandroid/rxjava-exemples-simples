package com.univlille.rxjavaexemplessimples;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private String TAG = "JC";

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // on crée un flux qui pourra être éliminé sans pb de fuite de mémoire (disposable) et qui
        // pourrait contenir plusieurs flux (composite)
        compositeDisposable
                // on ajoute le flux observable de Note
                .add(getNotesObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                // pour chaque Note reçue, on applique l'opérateur 'map' qui, ici, mettra la Note
                // en majuscule
                .map(new Function<Note, Note>() {
                    @Override
                    public Note apply(Note note) throws Exception {
                        // Making the note to all uppercase
                        note.setNote(note.getNote().toUpperCase());
                        return note;
                    }
                })
                // on abonne un Observateur 'disposable' à ce flux, ce qui met en route les processus
                // d'émission et de traitement des Notes
                .subscribeWith(getNotesObserver()));
    }

    private DisposableObserver<Note> getNotesObserver() {
        return new DisposableObserver<Note>() {

            @Override
            public void onNext(Note note) {
                Log.d(TAG, "Note: " + note.getNote());
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {
                Log.d(TAG, "All notes are emitted!");
            }
        };
    }

    // On crée un Observable à partir d'une liste de Notes. Cet Observable produit donc un flux
    // de Note (Observable<Note>) et non pas un flux de Notes (Observable<List<Note>>) !
    // Comme ce n'est pas un flux de données de type basique (String, int,...), on doit dire
    // comment créer ce flux.
    private Observable<Note> getNotesObservable() {
        final List<Note> notes = prepareNotes();
        // l'Observable est un flux de Note (ObservableOnSubscribe<Note>), il va donc émettre des 'Note'
        return Observable.create(new ObservableOnSubscribe<Note>() {
            // on indique comment le flux de l'Observable est créé. On définit donc les méthodes
            // permettant de produire le onNext et le onComplete.
            // Ainsi, pour chaque Note (c-a-d un émetteur de donnée), on indique ce qui est fait
            // pour chaque item, puis on indique ce qui est fait à la fin de l'émission des données

            @Override
            public void subscribe(ObservableEmitter<Note> emitter) throws Exception {
                // traitement à faire pour l'émission de chaque Note de la liste
                for (Note note : notes) {
                    if (!emitter.isDisposed()) {
                        emitter.onNext(note);
                    }
                }

                // traitement à faire à la fin de l'émission des Notes (si on ne fait pas ça, le
                // flux restera ouvert tant que l'application fonctionne !)
                if (!emitter.isDisposed()) {
                    emitter.onComplete();
                }
            }
        });
    }


    private List<Note> prepareNotes() {
        List<Note> notes = new ArrayList<>();
        notes.add(new Note(1, "acheter du pain"));
        notes.add(new Note(2, "passer au pressing"));
        notes.add(new Note(3, "faire la liste pour Noël"));
        notes.add(new Note(4, "faire le plein d'essence"));
        notes.add(new Note(5, "prévoir repas du Nouvel AN"));

        return notes;
    }

    class Note {
        int id;
        String note;

        public Note(int id, String note) {
            this.id = id;
            this.note = note;
        }

        public int getId() {
            return id;
        }

        public String getNote() {
            return note;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setNote(String note) {
            this.note = note;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}